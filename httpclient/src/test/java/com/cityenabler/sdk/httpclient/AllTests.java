package com.cityenabler.sdk.httpclient;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({InvokationTest.class})
public class AllTests {

}
