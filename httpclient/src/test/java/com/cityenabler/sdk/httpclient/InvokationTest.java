package com.cityenabler.sdk.httpclient;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Test;

import com.cityenabler.sdk.httpclient.impl.HttpClientImpl;
import com.cityenabler.sdk.httpclient.model.Bean;
import com.cityenabler.sdk.httpclient.model.InnerBean;

public class InvokationTest {
	
	private static final String mockurl = "http://localhost:8080/";
	
	private List<String> extractHeader(Bean resp, String headername){
		assertTrue(resp.getEchoHeaders().containsKey(headername));
		
		List<String> out = new ArrayList<String>();
		for(String h: resp.getEchoHeaders().get(headername)){
			out.addAll(Arrays.asList(h.split("\\s*,\\s*")));
		}
		
		return out;
	}
	
	@Test
	public void testGet() {
		HTTPClient client = new HttpClientImpl();
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		
		headers.put("fiware-service", Arrays.asList("myservice", "myservice2"));
		headers.put("fiware-servicepath", Arrays.asList("/mysubservice", "/mysubservice/2"));
		
		Bean resp = client.get(mockurl+"testget", headers, Bean.class);
		
		try{
			
			List<String> h = extractHeader(resp,"h_rcvd_fiware-service");
			assertTrue(h.contains("myservice"));
			assertTrue(h.contains("myservice2"));
			
			h = extractHeader(resp,"h_rcvd_fiware-servicepath");
			assertTrue(h.contains("/mysubservice"));
			assertTrue(h.contains("/mysubservice/2"));
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Test
	public void testPost() {
		HTTPClient client = new HttpClientImpl();
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		List<String> fiwareservice = new ArrayList<String>();
					fiwareservice.add("myservice");
					fiwareservice.add("myservice2");
		headers.put("fiware-service", fiwareservice);
		
		List<String> fiwareservicepath = new ArrayList<String>();
					fiwareservicepath.add("/mysubservice");
					fiwareservicepath.add("/mysubservice/2");
		headers.put("fiware-servicepath", fiwareservicepath);
		
		List<String> content = new ArrayList<String>();
					content.add(MediaType.APPLICATION_JSON);
		headers.put("Content-Type", content);
		
		InnerBean ib = new InnerBean();
		ib.setUsername("nino");
		ib.setPassword("s3cr3t");
		
		Bean resp = client.post(mockurl+"testpost", headers, ib, Bean.class);
		
		try{
			
			List<String> h = extractHeader(resp,"h_rcvd_fiware-service");
			assertTrue(h.contains("myservice"));
			assertTrue(h.contains("myservice2"));
			
			List<String> h1 =  extractHeader(resp,"h_rcvd_fiware-servicepath");
			assertTrue(h1.contains("/mysubservice"));
			assertTrue(h1.contains("/mysubservice/2"));
			
			List<String> h2 =  extractHeader(resp,"h_rcvd_content-type");
			assertTrue(h2.contains(MediaType.APPLICATION_JSON));
			
			assertTrue(resp.getEchoBody().getUsername().equals("nino"));
			assertTrue(resp.getEchoBody().getPassword().equals("s3cr3t"));
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Test
	public void testPut() {
		HTTPClient client = new HttpClientImpl();
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		
		List<String> fiwareservice = new ArrayList<String>();
					fiwareservice.add("myservice");
					fiwareservice.add("myservice2");
		headers.put("fiware-service", fiwareservice);
		
		List<String> fiwareservicepath = new ArrayList<String>();
					fiwareservicepath.add("/mysubservice");
					fiwareservicepath.add("/mysubservice/2");
		headers.put("fiware-servicepath", fiwareservicepath);
		
		List<String> content = new ArrayList<String>();
					content.add(MediaType.APPLICATION_JSON);
		headers.put("Content-Type", content);
		
		InnerBean ib = new InnerBean();
		ib.setUsername("nino");
		ib.setPassword("s3cr3t");
		
		Bean resp = client.put(mockurl+"testput", headers, ib, Bean.class);
		
		try{
			
			List<String> h = extractHeader(resp,"h_rcvd_fiware-service");
			assertTrue(h.contains("myservice"));
			assertTrue(h.contains("myservice2"));
			
			List<String> h2 = extractHeader(resp,"h_rcvd_fiware-servicepath");
			assertTrue(h2.contains("/mysubservice"));
			assertTrue(h2.contains("/mysubservice/2"));

			List<String> h3 = extractHeader(resp,"h_rcvd_content-type");
			assertTrue(h3.contains(MediaType.APPLICATION_JSON));
			
			assertTrue(resp.getEchoBody().getUsername().equals("nino"));
			assertTrue(resp.getEchoBody().getPassword().equals("s3cr3t"));
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Test
	public void testDelete() {
		HTTPClient client = new HttpClientImpl();
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		
		headers.put("fiware-service", Arrays.asList("myservice", "myservice2"));
		headers.put("fiware-servicepath", Arrays.asList("/mysubservice", "/mysubservice/2"));
		
		Bean resp = client.delete(mockurl+"testdelete", headers, Bean.class);
		
		try{
			
			List<String> h = extractHeader(resp,"h_rcvd_fiware-service");
			assertTrue(h.contains("myservice"));
			assertTrue(h.contains("myservice2"));
			
			h = extractHeader(resp,"h_rcvd_fiware-servicepath");
			assertTrue(h.contains("/mysubservice"));
			assertTrue(h.contains("/mysubservice/2"));
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Test
	public void testHead() {
		HTTPClient client = new HttpClientImpl();
		Map<String, List<String>> headers = new HashMap<String, List<String>>();
		
		Response r = client.head(mockurl+"testhead", headers, Response.class);
		assertEquals(r.getStatus(), 200);
		
	}
	
}
