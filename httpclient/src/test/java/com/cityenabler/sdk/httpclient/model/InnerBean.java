package com.cityenabler.sdk.httpclient.model;

public class InnerBean {
	
	private String username;
	private String password;
	
	public InnerBean(){
		
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
