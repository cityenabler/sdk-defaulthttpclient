package com.cityenabler.sdk.httpclient.model;

import java.util.List;
import java.util.Map;

public class Bean {

	private	Map<String, List<String>> echoHeaders;
	private InnerBean echoBody;
	
	public Bean(){
		
	}

	public Map<String, List<String>> getEchoHeaders() {
		return echoHeaders;
	}
	public void setEchoHeaders(Map<String, List<String>> echoHeaders) {
		this.echoHeaders = echoHeaders;
	}
	public InnerBean getEchoBody() {
		return echoBody;
	}
	public void setEchoBody(InnerBean echoBody) {
		this.echoBody = echoBody;
	}
}