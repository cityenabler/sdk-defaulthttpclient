package com.cityenabler.sdk.httpclient.impl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import com.cityenabler.sdk.httpclient.HTTPClient;
import com.fasterxml.jackson.databind.ObjectMapper;

public class HttpClientImpl implements HTTPClient{
	
	private static Client client = ClientBuilder.newClient();
	
	private static final boolean headers2lowercase = true;
	private ObjectMapper om = new ObjectMapper();
	
	private static MultivaluedMap<String, Object> map2MultivaluedMap(Map<String, List<String>> map){
		MultivaluedMap<String, Object> out = new MultivaluedHashMap<String, Object>();
		map.forEach((k, v) ->{
			out.put(headers2lowercase ? k.toLowerCase() : k, new ArrayList<Object>(v));
		});
		return out;
	}
	
	private static boolean isContentType(Entry<String, List<String>> e){
		return e.getKey().equalsIgnoreCase("content-type") 
				&& e.getValue() != null 
				&& !e.getValue().isEmpty()
				&& e.getValue().get(0) !=null
				&& (e.getValue().get(0) instanceof String);
	}
	
	private static <T> String inferBodyContentType(T body, Map<String, List<String>> headers){
		
		Optional<String> type = Optional.empty();
		for(Entry<String, List<String>> e : headers.entrySet()){
			if(isContentType(e)){
				type = Optional.of(e.getValue().get(0).toString());
				break;
			}
		}
		return type.orElse(MediaType.APPLICATION_JSON);
	}
	
	private static Invocation.Builder buildInvocationBuilder(String uri, Map<String, List<String>> headers){
		WebTarget wt = client.target(uri);
		Invocation.Builder ib = wt.request();
		
		Optional<Map<String, List<String>>> optHeaders = Optional.ofNullable(headers);
		if(optHeaders.isPresent()){
			ib.headers(map2MultivaluedMap(optHeaders.get()));
		}
		
		return ib;
	}
	
	public <T> T get(String uri, Map<String, List<String>> headers, Class<T> responseclass) {
		Invocation.Builder ib = buildInvocationBuilder(uri, headers);
		return ib.get(responseclass);
	}

	public <T, B> T post(String uri, Map<String, List<String>> headers, B body, Class<T> responseclass) {
		Invocation.Builder ib = buildInvocationBuilder(uri, headers);
		Entity<B> payload = Entity.entity(body, inferBodyContentType(body, headers));
		return ib.post(payload, responseclass);
	}

	public <T, B> T put(String uri, Map<String, List<String>> headers, B body, Class<T> responseclass) {
		
		Invocation.Builder ib = buildInvocationBuilder(uri, headers);
		Entity<B> payload = Entity.entity(body, inferBodyContentType(body, headers));
		return ib.put(payload, responseclass);
		
	}

	public <T> T delete(String uri, Map<String, List<String>> headers, Class<T> responseclass) {
		Invocation.Builder ib = buildInvocationBuilder(uri, headers);
		return ib.delete(responseclass);
	}

	@SuppressWarnings("unchecked")
	public <T> T head(String uri, Map<String, List<String>> headers, Class<T> responseclass) {
		if(responseclass != Response.class)
			throw new RuntimeException("HTTP HEAD Method must be invoked with "+Response.class.getName()+" as responseclass");
		
		Invocation.Builder ib = buildInvocationBuilder(uri, headers);
		return (T)ib.head();
	}

	@Deprecated
	public <B> B extractResponseEntity(Response response, Class<B> clazz) {
		
		StringBuffer result = new StringBuffer();
		try{
			BufferedReader rd = new BufferedReader(new InputStreamReader((InputStream) response.getEntity()));
			
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			
			return om.readValue(result.toString(), clazz);
		}
		catch(Exception e){
			//Do nothing and return empty payload
		}
		return null;
	}

}
